const request = require('request')
const cheerio = require('cheerio')
const get = require('lodash/get')

const fetch = (url, delay = 0) => new Promise((resolve, reject) => {
  request(url, (err, res, data) => {
    if (err || res.statusCode !== 200) {
      reject({ statusCode: get(res, 'statusCode', 500), err: err })
    } else {
      setTimeout(() => {
        return resolve(cheerio.load(data))
      }, delay)
    }
  })
})
const fetchAll = async (urls = [], delay = 0) => {
  return await Promise.all(urls.map(url => fetch(url, delay)))
}

module.exports = {
  Fetch: fetch,
  FetchAll: fetchAll,
}