const request = require('./request');
const cheerio = require('cheerio');
const fs = require('fs');
const prettier = require('prettier');

const baseURL = 'https://www.bankmega.com';
const filename = 'promo.json';

const writeFile = latestData => {
  fs.readFile(`./${filename}`, (err, data) => {
    if (err) {
      fs.writeFile(`./${filename}`, JSON.stringify(latestData), err => {
        if (err) console.log(err);
      });
    } else {
      fs.writeFile(
        `./${filename}`,
        JSON.stringify(latestData),
        (err, data) => {}
      );
    }
  });
};

request
  .Fetch(`${baseURL}/promolainnya.php`)
  .then(html => {
    let categories = {};
    const categoryPages = [];
    const scrpt = html('div#contentpromolain2 script')[0].children[0].data;
    const paths = scrpt.match(/load\("+[a-z.?=0-9&]+"\)/g);
    const titles = scrpt.match(/#+[a-z]+"/g);
    Object.keys(titles).forEach(i => {
      categoryPages.push({
        title: titles[i].replace(/["#]/g, ''),
        path: paths[i].match(/\?(.*)+"/)[1]
      });
    });
    categoryPages.forEach(i => {
      request
        .Fetch(`${baseURL}/promolainnya.php?${i.path}`)
        .then(categoryHtml => {
          const pageURLs = [];
          const products = [];
          categoryHtml('td>a.page_promo_lain').each((_i, pageHtml) => {
            pageURLs.push(
              `${baseURL}/promolainnya.php?product=${
                pageHtml.attribs.product
              }&subcat=${pageHtml.attribs.subcat}&page=${pageHtml.attribs.page}`
            );
          });
          request
            .FetchAll(pageURLs, 0)
            .then(res =>
              res.forEach(responseHtml => {
                responseHtml('ul#promolain>li').each((_liIndex, li) => {
                  const liHtml = cheerio.load(li);
                  let detailLink = `${liHtml('a').attr('href')}`;
                  if (!['http', 'www'].includes(detailLink)) {
                    detailLink = `${baseURL}/${detailLink}`;
                  }
                  let product = {
                    title: `${liHtml('img').attr('title')}`,
                    imgURL: `${baseURL}/${liHtml('img').attr('src')}`,
                    detailLink: detailLink
                  };
                  request
                    .Fetch(detailLink)
                    .then(detailHtml => {
                      product = Object.assign({}, product, {
                        detail: {
                          area: detailHtml('div.area').find('b').remove().text(),
                          periode: detailHtml('div.periode').find('b').remove().text(),
                        }
                      });
                      products.push(product);
                    })
                    .catch(_err => {
                      products.push(product);
                      console.log(
                        'failed fetch detail: ',
                        `${baseURL}/${liHtml('a').attr('href')}`
                      );
                    });
                  categories[i.title] = products;
                });
                writeFile(categories);
                const file = fs.readFileSync(`./${filename}`, 'utf8')
                prettier.format(file, {parser: 'json5'})
              })
            )
            .catch(err => console.log(err));
        })
        .catch(err => console.log('failed fetch: ', i.title));
    });
  })
  .catch(err => console.log(err));
