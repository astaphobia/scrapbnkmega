## Scraping promo list from `bankmega.com/promolainnya.php` with:

1. node v8.12.0
2. request v2.88.0
3. cheerio v1.0.0-rc.2
4. prettier v1.15.3

### the value is `promo.json` and would be prettify with prettier `{parser: 'json5'}`